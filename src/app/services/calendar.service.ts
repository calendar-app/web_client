import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SchedulerWeek } from 'src/app/models/week.model';

@Injectable({ providedIn: 'root' })
export class CalendarService {
  protected baseUrl = 'api';

  constructor(protected http: HttpClient) { }

  getCourseList(query?: any): Observable<string[]> {
    return this.http.get<string[]>(`${this.baseUrl}/courseList`, { params: query });
  }

  getScheduleByCourse(course: string, query?: any): Observable<SchedulerWeek[]> {
    return this.http.get<any[]>(`${this.baseUrl}/${course}`, { params: query })
      .pipe(
        map(weeks =>
          weeks.map(week => {
            week.weekNum = week._id;
            delete week._id;

            return new SchedulerWeek(week);
          })
        )
      );
  }
}
