import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent {
  @Input() errors;
  @Output() reload = new EventEmitter<any>();
}
