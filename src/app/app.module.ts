import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';
import { CourseListComponent } from './home/course-list/course-list.component';
import { EventItemComponent } from './home/scheduler/event-week/event-item/event-item.component';
import { EventWeekComponent } from './home/scheduler/event-week/event-week.component';
import { SchedulerComponent } from './home/scheduler/scheduler.component';
import { FooterComponent } from './footer/footer.component';

import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';

import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
registerLocaleData(localeIt, 'it');

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorPageComponent,
    LoadingPageComponent,
    CourseListComponent,
    EventItemComponent,
    EventWeekComponent,
    SchedulerComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'it-US' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
