import * as moment from 'moment';
moment.locale('it');

export class SchedulerItem {
  courseName?: string;
  timeStart?: string;
  timeEnd?: string;
  date?: string;
  required?: boolean;
  module?: string;
  professor?: string;
  room?: string;

  constructor(dayObj) {
    this.courseName = dayObj.courseName;
    this.timeStart = dayObj.timeStart;
    this.timeEnd = dayObj.timeEnd;
    this.date = dayObj.date;
    this.required = dayObj.required;
    this.module = dayObj.module;
    this.professor = dayObj.professor;
    this.room = dayObj.room;
  }
}

export class SchedulerEvent extends SchedulerItem {
  isOld: boolean;
  isOdd: boolean;
  isCurrentDay: boolean;
  isCurrentHour: boolean;
  timeStartH: number;
  timeEndH: number;

  constructor(dayObj: SchedulerItem) {
    super(dayObj);
  }
}
