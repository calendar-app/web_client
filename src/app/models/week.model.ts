import * as moment from 'moment';
import { SchedulerEvent } from './event.model';
moment.locale('it');

class Week {
  weekNum: number;
  days: SchedulerEvent[];
}

export class SchedulerWeek extends Week {
  startDate: Date;
  endDate: Date;

  constructor(weekObj: Week) {
    super();
    this.weekNum = weekObj.weekNum;
    this.startDate = moment().day('Lunedì').week(weekObj.weekNum).toDate();
    this.endDate = moment().day('Venerdì').week(weekObj.weekNum).toDate();
    this.days = weekObj.days.map(day => new SchedulerEvent(day));
  }
}
