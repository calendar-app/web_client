import { Component, ViewChild, ElementRef } from '@angular/core';
import { CourseListComponent } from './course-list/course-list.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  selectedCourse: string;
  errors: any;

  @ViewChild(CourseListComponent, { static: true }) courseListCmp: CourseListComponent;

  resetSelected() {
    this.courseListCmp.resetSelected();
  }

  reload() {
    this.courseListCmp.resetSelected();
    location.reload();
  }
}
