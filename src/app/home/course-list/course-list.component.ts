// tslint:disable: radix
import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { CalendarService } from '../../services/calendar.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {
  @Output() protected selectedCourseEmitter = new EventEmitter<string>();
  @Output() protected errorsEmitter = new EventEmitter<any>();

  selectedCourse: string;
  selectedCourse$ = new Subject<string>();

  courseList: string[];

  constructor(protected calendarService: CalendarService) { }

  ngOnInit() {
    this.selectedCourse$
      .subscribe(sel => {
        localStorage.setItem('selectedCourse', sel);
        this.selectedCourse = sel;
        this.selectedCourseEmitter.emit(sel);
      });

    // get course list from API
    this.calendarService.getCourseList()
      .subscribe(data => {
        this.courseList = data.sort((a, b) =>
          parseInt(a.split('-A')[1]) - parseInt(b.split('-A')[1]));

        const lsItem = localStorage.getItem('selectedCourse');
        if (lsItem && this.courseList.includes(lsItem)) {
          this.selectedCourse$.next(lsItem);
        }
      }, err => this.errorsEmitter.emit(err));
  }

  select(course) {
    this.selectedCourse$.next(course);
  }

  resetSelected() {
    localStorage.removeItem('selectedCourse');
    this.selectedCourse$.next();
    this.errorsEmitter.emit(null);
  }
}
