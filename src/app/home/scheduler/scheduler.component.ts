import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
import { delay, tap } from 'rxjs/operators';
import { SchedulerWeek } from '../../models/week.model';
import { CalendarService } from '../../services/calendar.service';
moment.locale('it');

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnChanges {
  @Input() selectedCourse: Input;
  @Output() errorsEmitter = new EventEmitter<any>();

  today = moment();
  // today = moment().add(1, 'day');
  isLoading = false;

  weeks: SchedulerWeek[] = [];

  constructor(private calendarService: CalendarService) { }

  ngOnChanges(changes: SimpleChanges) {
    this.isLoading = true;

    if (changes.selectedCourse.currentValue) {
      this.getWeeksByCourseName(changes.selectedCourse.currentValue);
    }
  }

  getWeeksByCourseName(course: string) {
    let startDate = moment(this.today);
    switch (startDate.day()) {
      case 6: // Saturday
        startDate = moment(startDate).add(2, 'day');
        break;
      case 0: // Sunday
        startDate = moment(startDate).add(1, 'day');
        break;
    }

    const startDay = moment(startDate).startOf('week').format('YYYY-MM-DD');
    this.calendarService
      .getScheduleByCourse(course, { startDay })
      .pipe(
        delay(300),
        tap(_ => this.isLoading = false),
      )
      .subscribe(
        weeks => this.weeks = weeks,
        error => this.errorsEmitter.emit(error));
  }
}
