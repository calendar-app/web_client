// tslint:disable: radix
import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { SchedulerEvent } from 'src/app/models/event.model';
import { SchedulerWeek } from 'src/app/models/week.model';
moment.locale('it');

@Component({
  selector: 'app-event-week',
  templateUrl: './event-week.component.html',
  styleUrls: ['./event-week.component.scss']
})
export class EventWeekComponent implements OnInit {
  @Input() week: SchedulerWeek;
  @Input() today: Date;
  currWeekNum: number;

  ngOnInit() {
    this.currWeekNum = moment(this.today).week();

    let counter = 0;
    this.week.days.map((day: SchedulerEvent, index: number) => {

      const date = moment(day.date).format('DD/MM/YYYY');
      const time = moment(day.timeEnd, 'H,mm').format('HH:mm');
      const lessonDateTime = moment(`${date} ${time}`, 'DD/MM/YYYY HH:mm');
      day.isOld = moment(lessonDateTime).isBefore(moment(this.today));

      if (!day.isOld && (index === 0 || day.date !== this.week.days[index - 1].date)) {
        counter++;
      }

      day.isOdd = counter % 2 === 0;

      day.isCurrentDay = moment(this.today)
        .isBetween(
          moment(day.date).startOf('day'),
          moment(`${date} ${time}`, 'DD/MM/YYYY HH:mm'));
      day.isCurrentHour = moment(this.today)
        .isBetween(
          moment(moment(day.date).format('DD/MM/YYYY') + day.timeStart, 'DD/MM/YYYY H:mm'),
          moment(moment(day.date).format('DD/MM/YYYY') + day.timeEnd, 'DD/MM/YYYY H:mm'));

      day.timeStartH = parseInt(day.timeStart.split(',')[0]);
      day.timeEndH = parseInt(day.timeEnd.split(',')[0]);
      return day;
    });
  }
}
