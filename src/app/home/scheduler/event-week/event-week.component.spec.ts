import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventWeekComponent } from './event-week.component';

describe('EventWeekComponent', () => {
  let component: EventWeekComponent;
  let fixture: ComponentFixture<EventWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
